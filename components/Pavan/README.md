<app-label [size]="'large'" [textCenter]='true'>Scroll Bar</app-label>

<br>

<div style="display: flex;justify-content: space-evenly;">
  <app-scrollpanel [heightInPixels]= "'150'" [widthInPixels] = "'250'" theme="danger">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
    doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
    doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
    doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
    doloribus quisquam nulla eveniet voluptatibus quaerat ratione.
  </app-scrollpanel>
  <app-scrollpanel [heightInPixels]= "'150'" [widthInPixels] = "'250'" theme="primary">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
    doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
    doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
    doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
    doloribus quisquam nulla eveniet voluptatibus quaerat ratione.
  </app-scrollpanel>
  <app-scrollpanel [heightInPixels]= "'150'" [widthInPixels] = "'250'" overflowX="true" theme="success">
    <p style="width: 300px;">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. 
      Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
      doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
      Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
      doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
      Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
      doloribus quisquam nulla eveniet voluptatibus quaerat ratione.  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
      Dicta blanditiis nostrum quibusdam temporibus neque facere harum aut hic, autem similique repellat vitae a
      doloribus quisquam nulla eveniet voluptatibus quaerat ratione.
    </p>
  </app-scrollpanel>
</div>

<br>

<app-label [size] = "'large'" [textCenter]='true'>Input Switch</app-label>

<br>
<div style="display: flex;justify-content: space-evenly;">
  <app-inputswitch round = "true" (emitter)='onChange($event)' theme='success' ></app-inputswitch>
  <app-inputswitch></app-inputswitch>
  <app-inputswitch theme ='danger' ></app-inputswitch>  
</div>
 
<br>
<app-label [size] = "'medium'" [textCenter]='true'>{{ change }}</app-label>
