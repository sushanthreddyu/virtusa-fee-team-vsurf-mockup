import { Component, OnInit, Input, Output ,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-inputswitch',
  templateUrl: './inputswitch.component.html',
  styleUrls: ['./inputswitch.component.css']
})
export class InputswitchComponent implements OnInit {

  @Input() theme:string = 'primary';
  @Input() round:boolean = false;
  @Input() checked:boolean = false;
  @Output() emitter = new EventEmitter<any>();


  constructor() { }

  ngOnInit(): void {
  }

  onChangeNotify(event){
    this.checked = !this.checked;
    this.emitter.emit({
      value:this.checked
    })
  }

}
