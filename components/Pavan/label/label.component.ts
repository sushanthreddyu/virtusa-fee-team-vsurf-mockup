import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.css']
})
export class LabelComponent implements OnInit {

  @Input() size:string="medium";
  @Input() inline:boolean = false;
  @Input() textCenter:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

}
