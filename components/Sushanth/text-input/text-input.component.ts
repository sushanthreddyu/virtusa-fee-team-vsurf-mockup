import { Component, OnInit, Input } from '@angular/core'

import { NgModel } from '@angular/forms'

@Component({
	selector: 'app-text-input',
	templateUrl: './text-input.component.html',
	styleUrls: ['./text-input.component.css'],
})
export class TextInputComponent implements OnInit {
	constructor() {}

	focus = false

	@Input() required = true
	@Input() value
	@Input() minLen = 4

	ngOnInit(): void {}
}
