# virtusa-FEE-team-vsurf-mockup

A Pseudo VSurf application mock up project to collaborate amongst FEE team and us e atoms and molecules from other team members.

## Project Structure

Place your components folder with you name as the folder name in the components folder in master branch.

- **components**:
    - member_name:
        - component-1
        - component-2
        - component-3

Make all your components as modules, and don't use any 3rd partty dependencies so that any other user can just use the code without breaking the component.

## Usage

Place the required component in a desired place in your project, and import the module where required. 

- If the module has an Angular component, use it's selector to use where required.
- If the module has an Angular directive, add the directive to the respective html tag.

## Components(member wise):

- **Sushanth**: 
	- Table
	- Charts/Graphs
	- InputText
- **Bhaskhar**:
	- Chips
	- Drag and drop
	- Auto Complete
- **Ritesh**:
	- Card
	- Carousal
	- Accordion 
- **Yashwanth**:
	- Button
	- Slider
	- Toggle
- **Pavan**:
	- ScrollPanel
	- InputSwitch
	- Label
- **Krishna**:
	- RadioButton
	- SelectButton
	- InputTextArea
- **Kalyan**:
    - Searchable Table
    - Sortable Table
    - List

> **NOTE: Members who are left to pick components, may do that and commit their components to the README. Please see that you don't overwrite other's data.**

> NOTE: These components have been selected to avoid overlap, but if you feel like making more components or components chosen by others please do so and add to your folder. All components are appreciated.
